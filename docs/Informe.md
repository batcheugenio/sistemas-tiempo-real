# Sistemas de Tiempo Real

En este informe se detallan las decisiones tomadas en el **trabajo final** y el modo de funcionamiento de la **simulación**. 

![Lenguaje Python](https://img.shields.io/badge/Lenguaje-python-blue.svg)

# Repositorio

## Organización

El repositorio se divide en 4 carpetas:

- **app**: contiene el módulo principal en donde se crean las instancias cada objeto.
- **docs**: contiene el informe, diagramas de clases y diagramas de flujo.
- **src**: contiene la definición de las clases.
- **tests**: contiene los archivos de pruebas unitarias para cada clase y  de pruebas de integración generales.

## Pipeline

El repositorio contiene un Runner de CI/CD configurado. Al hacer un push, el Runner se encarga de correr todos los tests presentes en el repositorio.

## Modo de desarrollo

El proyecto se desarrolló en Python, realizando pequeños commits. Para desarrollar se crearon branchs específicos por cada funcionalidad que se deseara agregar. Se crearon dos branches principales: **develop** y **master**. El branch de **develop** tiene como objetivo poder probar las funcionalidades antes de pasarlas a **master**. Asimismo, para pasar de develop a master se debe tener una aprobación del compañero. 


# Visión general

## Clases

Está compuesto por 6 clases principales:

|Clases                          |Función / Detalle                          |
|--------------------------|-------------------------------|
|Task          |`Clase que representa una tarea: un conjunto de acciones similares que se repiten a lo largo del tiempo`            |
|Scheduler                    |`Clase encargada de restringir el indeterminismo, seleccionando el orden de ejecución de las tareas que cumplan con las restricciones temporales. Su objetivo es asignarle a cada tarea el tiempo necesario en el procesador para poder realizar su ejecución. Es indiferente al código de la tarea y a sus entradas/salidas.`            |
|Procesador                    |`Clase encargada de recibir las tareas enviadas por el Scheduler y ejecutarlas en el tiempo indicado.`|
|Timer          |`Clase encargada de llevar el paso del tiempo de manera independiente.`|
|Plotter          |`Clase encargada de realizar los gráficos resultantes de la simulación.`|
|Simulator          |`Clase encargada de realizar la simulación.`|

## Detalle de los atributos de las clases

### ```1. Task```
 
* Datos de entrada:
	*   **name** : Nombre de la tarea.
	* **compute_time**: Tiempo de cómputo en el peor de los casos (este tiempo de cómputo se obtiene de analizar el código de la tarea; en este proyecto, se recibe como input al instanciar la tarea).
	* **deadline**: Plazo máximo de finalización. Es el tiempo máximo que puede transcurrir entre la activación de la tarea y su finalización u obtención de resultados.
	* **period**: Período de repetición. Cada T unidades de tiempo se vuelve a activar la tarea. 


* Atributos internos:
	* **priority**: Prioridad. En este caso, la prioridad será establecida por el Scheduler durante la planificación.
	* **executed_time**: Tiempo de cómputo ejecutado. Tiempo que pasó la tarea en ejecución en el procesador. Este tiempo será siempre menor al *compute_time*. Se utiliza para recordar cuánto tiempo se ejecutó esa tarea en caso de ser desalojada por otra con más prioridad.


### ```2. Processor```

* Datos de entrada:
	* **num**: Número de procesador. Por default, 0. 


* Atributos privados:
	* **busy**: Estado del procesador. Este atributo será cambiado al comenzar o concluir la ejecución de una tarea.
	*  **current_task**: Tarea en ejecución. Este atributo será establecido cuando una tarea entre en ejecución.


### ```3. Scheduler```
 
* Atributos privados:
	* **tasks**: Lista de tareas que el Scheduler va a ordenar y a enviar al procesador.
	*  **hyperperiod**: Hiperperíodo. Cada cuánto se va a repetir el comportamiento del sistema.


### ```4. Timer ```
 
* Atributos privados:
	* **final_time**: Duración total configurada en el timer.
	*  **initial_time**: Duración inicial configurada en el timer.

### ```5. Plotter ```
 
* Atributos privados:
	* **tasks_to_plot**: Tareas a graficar.
	* **start_task_times**: Inicio temporal del gráfico. Representa el inicio del eje x.
	* **end_task_times**: Finalización temporal del gráfico. Representa el fin del eje x.

### ```6. Simulator ```
 
* Atributos privados:
	* **tasks_list**: Listas de tareas con las cuales se hará la simulación.
	* **plotter**: Instancia de la clase Plotter que graficará el resultado de la simulación.


# Estrategia de planificación

## Tareas

En este desarrollo, el conjunto de tareas a ejecutar por el procesador es **estático**: siempre se van a ejecutar las mismas.
Con respecto a estas tareas, serán del tipo periódicas, es decir, que ocurren cada cierto intervalo de tiempo. 

## Algoritmo - Rate Monotonic

La política de tiempo real elegida para este Scheduler es **Rate Monotonic**. Durante la fase de diseño se le asigna una prioridad **base**, la cual se mantiene hasta el final de la fase de ejecución, lo que hace que este algoritmo maneje **prioridades estáticas**.

```mermaid
graph TD
    A[Políticas de tiempo real]
    A --> B(Cíclicos)
    A --> C(Prioridades)
    C --> D[Estáticas]
    D --> E(Rate Monotonic)
    D --> F(Deadline Monotonic)
    C --> G[Dinámicas]
    G --> H(Earliest Deadline First)
    G --> I(Least Laxity First)

    style C fill:#7454CA
    style D fill:#7454CA
    style E fill:#7454CA
```

Este algoritmo tiene las siguientes características:

* Durante la fase de diseño, a cada tarea se le asigna una prioridad invérsamente proporcional a su período.

> **Nota:** En el código, la asignación de la prioridad a cada tarea se puede observar en la función **sort_tasks_by_rate_monotonic** la cual toma las tareas y las ordena inversamente proporcional al período.

* D=T
* Durante la fase de ejecución, el planificador selecciona aquella tarea lista para ejecutarse que tenga mayor prioridad.
* La planificación es expulsiva. Ejemplo: 
	* La tarea T2 se encuentra lista para ejecutarse. 
	* La tarea T1 está ocupando el procesador.
	* Si la prioridad de la tarea T1 es mayor a la prioridad de la tarea T2, entonces continúa su ejecución hasta concluir.
	* En cambio, si la tarea T2 tiene una prioridad mayor a T1, entonces T1 libera el procesador y el planificador envía la tarea T2 a ejecutarse.

### Diagrama de flujo del funcionamiento con desalojo

<img src="docs/docs_diagrama-de-flujo.png" width="700" />

## Ejemplos de plotteo de la simulación

### Ejemplo 1
|Nombre Tarea          |Tiempo de ejecución          |Deadline          |Período          |
|--------------------------|-------------------------------|-------------------------------|-------------------------------|
|T1          |3|20|20|
|T2          |2|5|5|
|T3          |2|10|10|

<img src="docs/Figure_1.png" width="600" />

### Ejemplo 2
|Nombre Tarea          |Tiempo de ejecución          |Deadline          |Período          |
|--------------------------|-------------------------------|-------------------------------|-------------------------------|
|T1          |1|3|3|
|T2          |2|5|5|

<img src="docs/Figure_2.png" width="600" />

### Ejemplo 3
|Nombre Tarea          |Tiempo de ejecución          |Deadline          |Período          |
|--------------------------|-------------------------------|-------------------------------|-------------------------------|
|T1          |1|10|10|
|T2          |3|20|20|
|T3          |2|5|5|

<img src="docs/Figure_3.png" width="600" />

## Conclusiones teóricas

* Antes de realizar la planificación de las tareas se realiza el test de garantía, en donde se comprueba que todas las tareas llegarán a sus deadlines. Se asume que para cada tarea se considera el peor caso.
* El Rate Monotonic es el mejor algoritmo de planificación para tareas periódicas con prioridades estáticas. Todas las listas de tareas que no se puedan planificar mediante Rate Monotonic, no podrán ser planificadas por ningún otro algoritmo de planificación estático.
