pylint==2.6.0
flake8==3.8.4
matplotlib==3.3.3
numpy==1.19.0