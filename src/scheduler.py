from math import gcd

class Scheduler():
    def __init__(self, is_sorted_by_priority=False):
        self.tasks = None
        self.hyperperiod = 0
        self.is_sorted_by_priority = is_sorted_by_priority
        self.u_factor = 0

    def set_tasks(self, tasks):
        self.tasks = tasks
        self.set_hyperperiod()

    def sort_tasks_by_rate_monotonic(self, tasks):
        temp = sorted(tasks, key=lambda x: x.period, reverse=False)
        for idx, i in enumerate(temp):
            i.set_priority(idx)
        self.tasks = temp

    def is_schedulable(self):
        n = len(self.tasks)
        rate_mono_condition = n*((2**(1/n))-1)
        for i in self.tasks:
            self.u_factor += (i.compute_time/i.period)
        if self.u_factor <= rate_mono_condition:
            return True
        else:
            return False

    def set_hyperperiod(self):
        result = 0
        temp = []
        for i in self.tasks:
            temp.append(i.period)
        result = temp[0]
        for i in temp[1:]:
            result = result*i//gcd(result, i)
        self.hyperperiod = result
        
    def get_task_with_highest_priority(self):
        ready_tasks = [task for task in self.tasks if task.ready]
        if ready_tasks:
            return min(ready_tasks, key=lambda item: item.priority)

    def increment_period_for_all_tasks(self):
        for task in self.tasks:
            task.period_passed_time += 1
            # The task is activated when the period begins again.
            if task.period_passed_time == task.period:
                task.ready = True
                task.period_passed_time = 0
    
    def get_processor_use_percentage(self):
        return round(self.u_factor * 100, 2)