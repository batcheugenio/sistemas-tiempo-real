class Processor():
    def __init__(self, num=0, current_task=None):
        self.num=num
        self.busy=False
        self.current_task = current_task

    def execute(self, task, current_time):
        self.current_task = task
        self.current_task.executed_time += 1
        self.busy = True
        print(f"TIME: {current_time} - Processor: {self.num} is executing {task.executed_time}/{task.compute_time} the task: {task.name}")

    def check_task_finished(self, task):
        if task.executed_time == task.compute_time:
            self.flush()

    def flush(self):
        self.current_task.executed_time = 0
        self.current_task = None
        self.busy = False

    def idle(self, current_time):
        self.busy=False
        print(f"TIME: {current_time} - Processor: {self.num} is idle")

    def get_current_task(self):
        return self.current_task
