class Task():
    def __init__(self, name, compute_time, deadline, period, priority=1):
        self.name = name
        self.compute_time = compute_time
        self.deadline = deadline
        self.period = period
        self.priority = priority
        self.executed_time = 0
        self.period_passed_time = 0
        self.ready = True
    
    def set_priority(self, priority):
        self.priority = priority

    def get_priority(self):
        return self.priority
