from src.timer import Timer
from src.processor import Processor
from src.scheduler import Scheduler
from src.plotter import Plotter

class Simulator():
    def __init__(self, tasks_list, plotter, simulate_with_no_sorting=False):
        self.tasks_list = tasks_list
        self.plotter = plotter
        self.simulate_with_no_sorting = simulate_with_no_sorting

    def simulate(self):
        scheduler = Scheduler(self.simulate_with_no_sorting)
        processor = Processor()

        scheduler.set_tasks(self.tasks_list)
        # Now we should ask if the scheduler could 'schedule' tasks
        if scheduler.is_schedulable():
            hyperperiod = scheduler.hyperperiod
            # Then once we have the hyperperiod, we have to create a timer with hyperperiod as final time
            timer = Timer(hyperperiod)
            print(f"Processor: {processor.num} will start with {scheduler.get_processor_use_percentage()}% of capacity")
            # After that, we have to sort the tasks by the rate monotonic planification
            if not scheduler.is_sorted_by_priority:
                scheduler.sort_tasks_by_rate_monotonic(self.tasks_list)
            for time in range(timer.initial_time, timer.final_time):
                # Get the most priority task to execute
                high_priority_task = scheduler.get_task_with_highest_priority()
                if high_priority_task:
                    if not processor.busy: # Processor is free
                        processor.execute(high_priority_task, time)
                        self.plotter.add_task_to_plot(high_priority_task.name, time)
                        high_priority_task.ready = False
                        processor.check_task_finished(high_priority_task)
                    else:
                        current_task = processor.get_current_task()
                        if current_task.priority < high_priority_task.priority: # Check if we should preempt the current task 
                            processor.execute(current_task, time)
                            self.plotter.add_task_to_plot(current_task.name, time)
                            processor.check_task_finished(current_task)
                        else:
                            current_task.ready = True
                            processor.execute(high_priority_task, time)
                            self.plotter.add_task_to_plot(high_priority_task.name, time)
                            high_priority_task.ready = False
                            processor.check_task_finished(high_priority_task)
                else: # When there's any priority task to execute
                    if processor.busy:
                        current_task = processor.get_current_task()
                        processor.execute(current_task, time)
                        self.plotter.add_task_to_plot(current_task.name, time)
                        processor.check_task_finished(current_task)
                    else:
                        processor.idle(time)
                        self.plotter.add_task_to_plot('Idle', time)

                scheduler.increment_period_for_all_tasks()
                timer.sum()
        else:
            print("The task list given can't be executed. Please check the compute time and period of the tasks.")