import matplotlib.pyplot as plt
import numpy as np

class Plotter():
    def __init__(self):
        self.tasks_to_plot = []
        self.start_task_times = []
        self.end_task_times = []

    def add_task_to_plot(self, task_name, time):
        self.tasks_to_plot.append(f"{task_name}")
        self.start_task_times.append(time)
        self.end_task_times.append(time+1)

    def plot(self):
        """
        The scheduled results are displayed in the form of a
        gantt chart for the user to get better understanding
        """
        fig = plt.figure()
        ax = fig.add_subplot(111)
        # the data is plotted from_x to to_x along y_axis
        ax = plt.hlines(self.tasks_to_plot, self.start_task_times, self.end_task_times, linewidth=20, color = 'blue')
        plt.title('Sistemas de Tiempo Real - UNTREF\nRate Monotonic Scheduler')
        plt.grid(True)
        plt.xlabel("Real-Time clock")
        plt.ylabel("HIGH------------------Priority--------------------->LOW")
        plt.xticks(np.arange(min(self.start_task_times), max(self.end_task_times)+1, 1.0))
        plt.show()