# Trabajo Final Sistemas de Tiempo Real

## Consigna TP Final Sistemas de Tiempo Real

Modelar y desarrollar un simulador de sistemas de tiempo real que permita asignar tareas periódicas a un procesador homogéneo. 
Implementar un planificador a elección.  
Elaborar un informe explicando la estructura del proyecto y las decisiones que se tomaron respecto a la arquitectura. Fundamentación en base a la teoría.
Se acompaña una forma de corroborar la ejecución de la simulación: por ejemplo utilizando test cases o mostrando un log.

Preguntas disparadoras: ¿qué atributos debería tener una tarea? ¿y un procesador? ¿dónde se define el algoritmo de planificación desarrollado? ¿qué pasa cuando una tarea no puede ejecutarse?

Implementación de funcionalidades extra. Sugerencias: múltiples procesadores (mantenerlos homogéneos para no incrementar demasiado la complejidad), tareas aperiódicas, configuraciones en el algoritmo de planificación o un algoritmo distinto, estadísticas, cambio de contexto, uso de un recurso (no lo recomiendo porque puede volverse muy complejo).


## <a href="/docs/Informe.md"> Informe </a>
