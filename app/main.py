import sys
sys.path.append(".")
from src.task import Task
from src.plotter import Plotter
from src.simulator import Simulator
from testcases import Test

if __name__ == "__main__":
    test_simulator = Test()
    # test_simulator.run_example_1()
    # test_simulator.run_example_2()
    # test_simulator.run_example_3()
    # test_simulator.run_example_4()
    test_simulator.run_example_5()