import sys
sys.path.append(".")

from src.timer import Timer
from src.processor import Processor
from src.scheduler import Scheduler
from src.plotter import Plotter
from src.task import Task
from src.simulator import Simulator

class Test():
    def run_example_1(self):
        task1 = Task("T1", 3, 20, 20)
        task2 = Task("T2", 2, 5, 5)
        task3 = Task("T3", 2, 10, 10)
        tasks_list = [task1, task2, task3]

        plotter = Plotter()
        simulator = Simulator(tasks_list, plotter)
        simulator.simulate()
        plotter.plot()

    def run_example_2(self):
        task1 = Task("T1", 1, 3, 3)
        task2 = Task("T2", 2, 5, 5)
        tasks_list = [task1, task2]

        plotter = Plotter()
        simulator = Simulator(tasks_list, plotter)
        simulator.simulate()
        plotter.plot()

    def run_example_3(self):
        task1 = Task("T1", 1, 10, 10)
        task2 = Task("T2", 3, 20, 20)
        task3 = Task("T3", 2, 5, 5)
        tasks_list = [task1, task2, task3]

        plotter = Plotter()
        simulator = Simulator(tasks_list, plotter)
        simulator.simulate()
        plotter.plot()
    
    # Failed test case
    def run_example_4(self):
        task1 = Task("T1", 25, 50, 50)
        task2 = Task("T2", 30, 75, 75)
        tasks_list = [task1, task2]

        plotter = Plotter()
        simulator = Simulator(tasks_list, plotter)
        simulator.simulate()
    
    def run_example_5(self):
        task1 = Task("T1", 1, 3, 5)
        task2 = Task("T2", 2, 5, 5)
        tasks_list = [task1, task2]

        plotter = Plotter()
        simulate_sorting_by_priority = True
        simulator = Simulator(tasks_list, plotter, simulate_sorting_by_priority)
        simulator.simulate()
        plotter.plot()