import sys
sys.path.append(".")
import unittest
from src.plotter import Plotter
from src.task import Task
from src.simulator import Simulator
from unittest.mock import patch, call

class TestSimulator(unittest.TestCase):
    
    @patch('builtins.print')
    def test_success_simulation(self, mock_print):
        task1 = Task("T1", 1, 3, 5)
        task2 = Task("T2", 2, 5, 5)
        tasks_list = [task1, task2]
        plotter = Plotter()
        simulator = Simulator(tasks_list, plotter)
        simulator.simulate()
        assert mock_print.mock_calls == [call('Processor: 0 will start with 60.0% of capacity'),
                                         call('TIME: 0 - Processor: 0 is executing 1/1 the task: T1'), 
                                         call('TIME: 1 - Processor: 0 is executing 1/2 the task: T2'),
                                         call('TIME: 2 - Processor: 0 is executing 2/2 the task: T2'),
                                         call('TIME: 3 - Processor: 0 is idle'),
                                         call('TIME: 4 - Processor: 0 is idle')]
    
    @patch('builtins.print')
    def test_failed_simulation(self, mock_print):
        task1 = Task("T1", 4, 3, 5)
        task2 = Task("T2", 2, 5, 5)
        tasks_list = [task1, task2]
        plotter = Plotter()
        simulator = Simulator(tasks_list, plotter)
        simulator.simulate()
        mock_print.assert_called_with("The task list given can't be executed. Please check the compute time and period of the tasks.")
    
    @patch('builtins.print')
    def test_simulate_sorting_by_priority(self, mock_print):
        task1 = Task("T1", 1, 3, 5, 1)
        task2 = Task("T2", 2, 5, 5, 0)
        tasks_list = [task1, task2]
        plotter = Plotter()
        sort_by_priority = True
        simulator = Simulator(tasks_list, plotter, sort_by_priority)
        simulator.simulate()
        assert mock_print.mock_calls == [call('Processor: 0 will start with 60.0% of capacity'),
                                         call('TIME: 0 - Processor: 0 is executing 1/2 the task: T2'), 
                                         call('TIME: 1 - Processor: 0 is executing 2/2 the task: T2'),
                                         call('TIME: 2 - Processor: 0 is executing 1/1 the task: T1'),
                                         call('TIME: 3 - Processor: 0 is idle'),
                                         call('TIME: 4 - Processor: 0 is idle')]