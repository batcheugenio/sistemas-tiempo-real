import sys
sys.path.append(".")
import unittest

from src.scheduler import Scheduler
from src.task import Task

class SchedulerAttributesTests(unittest.TestCase):
    def test_scheduler_creation(self):
        my_scheduler = Scheduler()
        self.assertEqual(my_scheduler.hyperperiod, 0)

class SchedulerMethodsTests(unittest.TestCase):
    def test_scheduler_creation(self):
        my_scheduler = Scheduler()
        task1 = Task("MockName", 1, 12, 12, 0)
        task2 = Task("MockName", 2, 5, 5, 0)
        tasks = [task1, task2]
        my_scheduler.set_tasks(tasks)
        self.assertEqual(my_scheduler.tasks, tasks)

    def test_scheduler_sorting(self):
        my_scheduler = Scheduler()
        task1 = Task("MockName", 1, 12, 12, 0)
        task2 = Task("MockName", 2, 5, 5, 0)
        tasks = [task1, task2]
        my_scheduler.sort_tasks_by_rate_monotonic(tasks)
        self.assertEqual(my_scheduler.tasks, [task2, task1])
    
    def test_scheduler_is_assigning_prority_to_tasks(self):
        my_scheduler = Scheduler()
        task1 = Task("MockName", 1, 12, 12, 0)
        task2 = Task("MockName", 2, 5, 5, 0)
        tasks = [task1, task2]
        my_scheduler.sort_tasks_by_rate_monotonic(tasks)
        self.assertEqual(task1.priority, 1)
    
    def test_tasks_are_schedulable(self):
        my_scheduler = Scheduler()
        task1 = Task("MockName", 85, 1, 12, 0)
        task2 = Task("MockName", 60, 1, 5, 0)
        tasks = [task1, task2]
        my_scheduler.set_tasks(tasks)
        self.assertFalse(my_scheduler.is_schedulable())
    
    def test_get_hyperperiod(self):
        my_scheduler = Scheduler()
        task1 = Task("MockName", 1, 1, 12, 0)
        task2 = Task("MockName", 2, 1, 6, 0)
        tasks = [task1, task2]
        my_scheduler.set_tasks(tasks)
        my_scheduler.set_hyperperiod()
        self.assertEqual(my_scheduler.hyperperiod, 12)
    


