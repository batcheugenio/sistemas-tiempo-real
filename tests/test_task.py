import sys
sys.path.append(".")
import unittest
from src.task import Task

class TaskAttributesTests(unittest.TestCase):
    def test_task_creation(self):
        task = Task("MockName", 1, 12, 12, 0)
        self.assertEqual(task.name, "MockName")
    
    def test_setting_priority_to_task(self):
        task = Task("TestCase", 1, 12, 12, 0)
        task.set_priority(3)
        self.assertEqual(task.priority, 3)
    
    def test_task_has_executed_time(self):
        task = Task("TestCase", 1, 12, 12, 0)
        self.assertEqual(task.executed_time, 0)
