import sys
sys.path.append(".")
import unittest
from unittest.mock import patch
from src.task import Task
from src.processor import Processor

class ProcessorAttributesTests(unittest.TestCase):
    def test_processor_creation(self):
        task1 = Task("Task1", 3, 10, 10)
        proc = Processor(4)
        self.assertEqual(proc.num, 4)
    
    def test_execute_one_task(self):
        task1 = Task("Task1", 3, 10, 10)
        current_time = 1
        proc = Processor()
        proc.execute(task1, current_time)
        self.assertEqual(proc.busy, True)

    @patch('builtins.print')
    def test_execute_one_task_and_get_log_message(self, mock_print):
        task1 = Task("Task1", 3, 10, 10)
        current_time = 1
        proc = Processor(1)
        proc.execute(task1, current_time)
        mock_print.assert_called_with("TIME: 1 - Processor: 1 is executing 1/3 the task: Task1")

         